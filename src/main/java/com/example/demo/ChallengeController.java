package com.example.demo;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ChallengeController {
	final Logger logger = Logger.getLogger(ChallengeController.class);

	@RequestMapping(value="/person", method = RequestMethod.POST)
	public ResponseEntity<Person> persistPerson(@RequestBody Person person) {
		
		logger.info("person name is "+ person.getFirstName());
		if (person.getFirstName().equals("Manikandan")) {
			return ResponseEntity.ok(person);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}

}
